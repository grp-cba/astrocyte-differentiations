# Analysis workflow

## Overview

- Identify nuclei based on DAPI signal, DNA stain, 3rd channel
- Define region around each nucleus, i.e. nuclear periphery, DNA stain, 3rd channel
- For each nuclear periphery **measure** mean intensity in the astrocyte marker (GFAP, 2nd channel) staining
    - This signal signifies that the cells became astrocytes.
- For each nucleus **measure** within the nucleus the mean in the proliferation marker (ki67, 1st channel)
    - Signal indicates proliferation
    - If cells differentiate to become astrocytes they don't proliferate, so this signal should be absent

## Notes

- The below instructions are taylored to work at EMBL Heidelberg, please [contact us](mailto:mailto:image-analysis-support@embl.de) in case you need support running the analysis in your compute infrastructure.

## Image data conversion to OME.TIFF

- [Log into the EMBL computer cluster](https://git.embl.de/grp-cba/cba/-/blob/master/resources_access/using-embl-cluster.md?ref_type=heads#using-the-embl-hpc)
    - `ssh USERNAME@login01.cluster.embl.de`

Run the below conversion code, replacing the paths.

```bash
source /g/cba/miniconda3/etc/profile.d/conda.sh
conda activate /g/cba/miniconda3/envs/file-formats
batchconvert set_default_param workdir /scratch/$USER/.batchconvert
batchconvert set_default_param cluster_options "--nodes=10 --ntasks-per-node=8 --cpus-per-task=1 --mem=24G"
batchconvert ometiff -pf cluster -p .vsi "/g/heard/a_ritz/Microscopy/20231101_CR_AD_IF_GFAPki67" "/g/heard/a_ritz/Microscopy/20231101_CR_AD_IF_GFAPki67/OME-TIFF"
```

## Image Analysis

<details>
<summary>Detailed instructions</summary>
Processing parameters:
- `inputPattern` is a text that specifies all the image files that should be analysed; to select multiple file you can use wildcards, e.g. `/g/heard/a_ritz/Microscopy/20231101_CR_AD_IF_GFAPki67/OME-TIFF/*.tif` would analyse all the files in `/g/heard/a_ritz/Microscopy/20231101_CR_AD_IF_GFAPki67/OME-TIFF` that end with `.tif`.
- `ringWidth` is a number that specifies the width (pixel units) of the border region around the nuclei in which the GFAP signal will be measured (we started the project using a value of 10)
</details>

The below instructions run on a small example data set that is stored on a group server accessible to everyone at EMBL: `/g/cba/exchange/astrocyte-differentiations-data`. For public access, the same data is also staged at `https://s3.embl.de/astrocyte-differentiations/input-data`. Please adapt the parameters to run it on other data.  

- Log into the cluster: `ssh USERNAME@login01.cluster.embl.de`
    - [Instructions](https://git.embl.de/grp-cba/cba/-/blob/master/resources_access/using-embl-cluster.md?ref_type=heads#using-the-embl-hpc) if you are using the EMBL computer cluster for the first time.
- **Just once** fetch the code repository: `git clone git@git.embl.de:grp-cba/astrocyte-differentiations.git`
- **Every time** fetch the latest analysis code: `cd astrocyte-differentiations; git pull; cd ~`
- `module load Nextflow`
- `nextflow astrocyte-differentiations/code/nextflow/measure_differentiation.nf --inputPattern "/g/cba/exchange/astrocyte-differentiations-data/*.ome.tiff" --outputDir "/g/cba/exchange/astrocyte-differentiations-data/results" --ringWidth 10 --pythonScript "astrocyte-differentiations/code/python/segment_and_measure.py" -c "astrocyte-differentiations/code/nextflow/nextflow.config" -profile slurm`
    - **Adapt the following parameters to run on your data** 
        - `inputPattern`, `outputDir`, `ringWidth`
        - Tip: copy and paste the above code into a text editor, change the parameters there, and then copy into the terminal and hit enter.

## Output table concatenation

[Table concatentation instructions](https://git.embl.de/heriche/image-data-explorer/-/wikis/Preparing-the-data-for-use-with-the-IDE)

The tabular analysis results (s.a.) can be recursively concatenated inside a Linux or Mac terminal using the below command (**adapt the paths**):

```
find . -type f -name " /g/heard/a_ritz/Microscopy/20231101_CR_AD_IF_GFAPki67/analysis/*.tsv" -exec awk 'NR==1 || FNR!=1' {} + > /g/heard/a_ritz/Microscopy/20231101_CR_AD_IF_GFAPki67/analysis/concatenated.tsv
```

## Visual QC and statistical analysis

The concatentated table (s.a.) can be used to visually inspect all analysis results [using MoBIE](https://git.embl.de/grp-cba/cba/-/blob/master/resources_access/using-mobie.md):

- Open MoBIE/Fiji
- In the Fiji search bar type: `open table` and hit run.
- Enter the below parameters in the UI (**adapt the paths**)

```
final OpenTableCommand command = new OpenTableCommand();
command.root = new File( "/Volumes/20231101_CR_AD_IF_GFAPki67/analysis" );
command.table = new File( "/Volumes/20231101_CR_AD_IF_GFAPki67/analysis/concatenated.tsv" );
command.images = "DAPI_Path=DAPI,ki67_Path=ki67,GFAP_Path=GFAP";
command.labels = "Nuclei_Labels_Path=Nuclei,Nuclei_Periphery_Labels_Path=Periphery";
command.removeSpatialCalibration = true;
```

## Dependencies

This is **not needed** for just running the workflow, this is only for developers.

### Install locally

- `mamba env create -f environment.yml`

### Install on /g/cba

- [Log into the EMBL computer cluster](https://git.embl.de/grp-cba/cba/-/blob/master/resources_access/using-embl-cluster.md?ref_type=heads#using-the-embl-hpc)
    - `ssh USERNAME@login01.cluster.embl.de`
- `source /g/cba/miniconda3/init_conda.sh`
- `conda env create -f environment.yml -p /g/cba/miniconda3/envs/astrocyte-differentiations`


## References

- [Minimal overview about using Nextflow for bioimage analysis](https://docs.google.com/presentation/d/1QD-0t6_Us5bVsH_e1CWOoS4nw6tNK41IG2O2CmzYyhI/edit?usp=sharing)
- [The Image Data Explorer on GitLab](https://git.embl.de/heriche/image-data-explorer#image-data-explorer)
- [Muller C, Serrano-Solano B, Sun Y, Tischer C, Hériché JK. The Image Data Explorer: Interactive exploration of image-derived data. PLoS One. 2022 Sep 15;17(9):e0273698. doi: 10.1371/journal.pone.0273698. PMID: 36107835; PMCID: PMC9477257.](https://pubmed.ncbi.nlm.nih.gov/36107835/)
- [Di Tommaso, P., Chatzou, M., Floden, E. et al. Nextflow enables reproducible computational workflows. Nat Biotechnol 35, 316–319 (2017). https://doi.org/10.1038/nbt.3820](https://www.nature.com/articles/nbt.3820)
- [Pape, C., Meechan, K., Moreva, E. et al. MoBIE: a Fiji plugin for sharing and exploration of multi-modal cloud-hosted big image data. Nat Methods 20, 475–476 (2023). https://doi.org/10.1038/s41592-023-01776-4](https://www.nature.com/articles/s41592-023-01776-4)


