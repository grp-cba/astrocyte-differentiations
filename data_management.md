# Data managment

Description of the management of both the microscopy **input** and analysis **output** data.

- [General data management considerations](https://git.embl.de/grp-cba/cba#data-management)

## Data location(s)

The following folder have been shared via the DMA:

- `/g/heard/a_ritz/Microscopy/20231101_CR_AD_IF_GFAPki67/`

### Minimal example data 

Minimal example data (already converted from VSI to OME-TIFF) is available at:

- `/g/cba/exchange/astrocyte-differentiations-data`
- https://s3.embl.de/astrocyte-differentiations/input-data

## File and folder naming schemes

- example file: `npc_I43_60x_647ki67_488GFAP_05.vsi`
- naming scheme: `${cellType}_${cellLine}_${magnification}_${channel1}_${channel2}_${image}`
- example file: `CRADI431d_b_d3_60x_647ki67_488GFAP_06.vsi`
- naming scheme: `${ExperimenCelltypeCellLineCondition}_${replicate}_${differentiationDay}_${magnification}_${channel1}_${channel2}_${image#}`


note: all files starting with "CRAD" refer to astrocyte differentiations. All files starting with "npc" are neuronal progenitor cells (NPCs) that were used as a control (NPCs were differnitated into astrocytes)

## Image file types

All files are Olympus VSI.
