# Instructions for batch converting vsi files to OME-TIFF and OME-Zarr

Activate the conda environment where relevant file converters are installed:

```bash
source /g/cba/miniconda3/etc/profile.d/conda.sh
conda activate /g/cba/exchange/Oezdemir/service/envs/file_formats
```

In the active environment, you can use the `batchconvert` tool to convert a collection of vsi files to OME-TIFF format. 
The simplest command for converting a collection of vsi files to the OME-TIFF format is as follows:

```bash
batchconvert ometiff "path/to/input/folder/*vsi" path/to/output/folder
```

Replace the `path/to/input/folder` with the actual path to the directory that contains the vsi files.
The wildcard pattern `/*vsi` is needed to exclusively select the vsi files for conversion in case the input folder also 
contains files with different formats. So if you know that all the files in the input folder are in vsi format, then 
you can remove `/*vsi` from the input path. 
 
Also, please note that double quotes ("") around the input path are mandatory as long as the input path has a wildcard pattern. 

Converting to OME-Zarr is very similar. Just replace `ometiff` with `omezarr` in the command line.

### Try on test-data

To convert test data to OME-TIFF, run the following command line in the active environment:
```bash
batchconvert ometiff /g/cba/exchange/Oezdemir/data/shared/vsi /g/cba/exchange/Oezdemir/data/shared/OME-TIFF
```

To convert a selection of the test data to OME-Zarr, run the following command line in the active environment:
```bash
batchconvert omezarr -rz 3 "/g/cba/exchange/Oezdemir/data/shared/vsi/*npc*" /g/cba/exchange/Oezdemir/data/shared/OME-Zarr
```

Note that the first command line converts all files in the input directory to OME-TIFF, whereas the second command line
converts only the files that contain the pattern `npc` in the filename to OME-Zarr.

