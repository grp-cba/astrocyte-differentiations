# Change Log
All notable changes to the code in this project will be documented in this file.
 
The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.2.0] - 2023-11-11
  
### Added

- The width of the ring that we create around the nucleus is now exposed as a parameter.
- The width of the ring has been added to the output table
- The script version has been added to the output table
 
### Changed

- The thresholding method has been changed from Otsu to Li; Li is producing slightly lower thresholds, which for the current data seems to produce better results.
 
### Fixed
