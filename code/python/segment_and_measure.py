# Dependencies
# https://git.embl.de/grp-cba/astrocyte-differentiations/-/blob/main/environment.yml

# How to call this script, example:
# python segment_and_measure.py
#   --file_path "/Users/tischer/Desktop/alina/npc_I3_60x_647ki67_488GFAP_01.tif"
#   --output_dir "/Users/tischer/Desktop/alina/output"
#   --ring_width 10

# Example data:
# "/Users/tischer/Desktop/alina/input/CRADI31da_d3_60x_647ki67_488GFAP_01_s01.tif"
# "/Users/tischer/Desktop/alina/input/CRADI31d_a_d3_60x_647ki67_488GFAP_01.ome.tiff"
# "/Volumes/cba/exchange/Oezdemir/data/share/alina/OME-TIFF/CRADI31d_a_d3_60x_647ki67_488GFAP_01.ome.tiff"


import argparse
import numpy as np
import pandas as pd
import skimage.filters
from skimage.io import imread
from skimage.measure import label, regionprops
from scipy.ndimage import binary_fill_holes
from skimage.segmentation import expand_labels
import os
from tifffile import imwrite

__version__ = '0.2.0'

# Argument parsing
parser = argparse.ArgumentParser(description='Astrocyte differentiation analysis')
parser.add_argument('file_path', type=str)
parser.add_argument('output_dir', type=str)
parser.add_argument('nuclear_ring_width', type=int, default=10)
parser.add_argument('-v', '--version', action='version', version=f'%(prog)s {__version__}')

args = parser.parse_args()
input_image_path = args.file_path
nuclear_ring_width = args.nuclear_ring_width
output_dir = args.output_dir

# Create output directories
try:
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
except Exception as e:
    # the error catching appears to be necessary due to concurrency issues when
    # running this with nextflow
    print(f"Warning: {e}")

# Fixed parameters
minimal_diameter = 50  # pixel units; more filtering can be done during statistical analysis
dapi_channel = 2  #
gfap_channel = 1  # for outside nucleus measurement
ki67_channel = 0  # for inside nucleus measurement

# Open the image
print("Opening:", input_image_path)
import time

start_time = time.time()
image = imread(input_image_path)
print(image.shape)
end_time = time.time()
execution_time = end_time - start_time
print(f"Opening the image took: {execution_time} seconds")

# Perform maximum projection along the first (z) axis
max_projection = np.max(image, axis=0)

# Segment nuclei
# ...could use CellPose instead
threshold = skimage.filters.threshold_li(max_projection[:, :, dapi_channel])
binary_image = max_projection[:, :, dapi_channel] > threshold
binary_image = binary_fill_holes(binary_image)
label_image = label(binary_image)

# Filter small objects
filtered_objects = []
for region in regionprops(label_image):
    if region.equivalent_diameter > minimal_diameter:
        filtered_objects.append(region.label)
filtered_binary_image = np.isin(label_image, filtered_objects)
nuclei_labels = label(filtered_binary_image)

# Create rings around nuclei

expanded_labels = expand_labels(nuclei_labels, nuclear_ring_width)
nuclei_periphery_labels = expanded_labels - nuclei_labels

# Measure intensity in the different channels

# Measure inside nucleus
props_inside = regionprops(nuclei_labels, intensity_image=max_projection[:, :, ki67_channel])

# Measure properties of labeled regions
props_periphery = regionprops(nuclei_periphery_labels, intensity_image=max_projection[:, :, gfap_channel])

# Initialize lists to hold the data
label = 'label'  # MoBIE column name: Do not change!
area_nucleus = 'Area_Nucleus'
mean_inside = 'Mean_Intensity_Inside_ki67'
area_periphery = 'Area_Periphery'
mean_periphery = 'Mean_Intensity_Periphery_GFAP'

data = {
    label: [],
    'centroid-0': [],  # MoBIE column name: Do not change!
    'centroid-1': [],  # MoBIE column name: Do not change!
    area_nucleus: [],
    mean_inside: [],
    area_periphery: [],
    mean_periphery: []
}

# Loop through the properties and add the data to the lists
for prop_inside, prop_periphery in zip(props_inside, props_periphery):
    data[label].append(prop_inside.label)
    data["centroid-0"].append(prop_inside.centroid[0])
    data["centroid-1"].append(prop_inside.centroid[1])
    data[area_nucleus].append(prop_inside.area)
    data[mean_inside].append(prop_inside.mean_intensity)
    data[area_periphery].append(prop_periphery.area)
    data[mean_periphery].append(prop_periphery.mean_intensity)

# Convert the lists into a Pandas DataFrame
df = pd.DataFrame(data)

# Save results
#

basename = os.path.basename(input_image_path)
basename_noext = os.path.splitext(basename)[0]

# Save the nuclei_labels and nuclei_periphery_labels as TIFF files
nuclei_labels_path = os.path.join(output_dir, f"{basename_noext}_nuclei_labels.tif")
nuclei_periphery_labels_path = os.path.join(output_dir, f"{basename_noext}_nuclei_periphery_labels.tif")
dapi_path = os.path.join(output_dir, f"{basename_noext}_DAPI_max.tif")
ki67_path = os.path.join(output_dir, f"{basename_noext}_ki67_max.tif")
gfap_path = os.path.join(output_dir, f"{basename_noext}_GFAP_max.tif")

imwrite(nuclei_labels_path, nuclei_labels.astype(np.uint16))
imwrite(nuclei_periphery_labels_path, nuclei_periphery_labels.astype(np.uint16))
imwrite(dapi_path, max_projection[:, :, dapi_channel].astype(np.uint16))
imwrite(ki67_path, max_projection[:, :, ki67_channel].astype(np.uint16))
imwrite(gfap_path, max_projection[:, :, gfap_channel].astype(np.uint16))

# Add paths relative to output directory
df['DAPI_Path'] = os.path.basename(dapi_path)
df['ki67_Path'] = os.path.basename(ki67_path)
df['GFAP_Path'] = os.path.basename(gfap_path)
df['Nuclei_Labels_Path'] = os.path.basename(nuclei_labels_path)
df['Nuclei_Periphery_Labels_Path'] = os.path.basename(nuclei_periphery_labels_path)
df['Version'] = __version__
df['Nuclear_Ring_Width'] = nuclear_ring_width

# Add condition
pos = basename.find("_60x_")
condition = basename[:pos] if pos != -1 else "parsing ERROR"
df['Condition'] = condition

# Save the DataFrame as a CSV file
# Save the DataFrame as a TSV file
df_path = os.path.join(output_dir, f"{basename_noext}_measurements.tsv")
df.to_csv(df_path, index=False, sep='\t')

print("Results saved to", output_dir)
print("Analysis of", input_image_path, "is done!")
