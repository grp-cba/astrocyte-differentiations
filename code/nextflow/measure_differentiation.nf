#!/usr/bin/env nextflow

// nextflow run astrocyte_differentiations.nf --inputPattern "/g/heard/a_ritz/Microscopy/20231101_CR_AD_IF_GFAPki67/IJ-TIFF/*_s01.tif" --outputDir "/g/heard/a_ritz/Microscopy/20231101_CR_AD_IF_GFAPki67/analysis" --pythonScript "astrocyte-differentiations/code/python/segment_and_measure.py" --ringWidth 10

pythonScript = new File(params.pythonScript).getAbsolutePath()
outputDir = new File(params.outputDir).getAbsolutePath()

process ANALYSE {
    input:
    path image

    output:
    path "done"

    debug true

    conda '/g/cba/miniconda3/envs/astrocyte-differentiations'

    // This immediately puts everything into the outputDir
    // better practice may be to first store it locally and then
    // use the publishDir directive to copy it to the final location
    // however here since the analysis is only a single step,
    // maybe what we do is fine...
    script:
    """
    which python
    python ${pythonScript} $image ${outputDir} ${params.ringWidth} 
    touch done 
    """
}

workflow {
    images = Channel.fromPath("${params.inputPattern}")
    ANALYSE(images)
}

