params.csv = "/g/cba/exchange/astrocyte-differentiations-data/labid/data_nf.csv"

process INSPECT_IMAGE {
    debug true
    input:
    tuple val(sampleId), file(image)

    script:
    """
    echo "ID: " $sampleId
    echo "Path: " $image
    module load ImageMagick
    identify $image
    """
}

workflow {
    images = Channel.fromPath(params.csv)
        | splitCsv(header: true)
        | map { row -> tuple(row['Dataset ID'], file(row['FilePath'])) }
    INSPECT_IMAGE(images)
}

/**
# go to cluster
hpc

# fetch data table from labID
/g/funcgen/bin/labid export study -s 02201756-aa76-43f5-894a-1e859c2a4a1e -o /g/cba/exchange/astrocyte-differentiations-data/labid -f data.csv --format tabular

# remove duplicate FilePath entries, which are due to the single channel data model of LabId
# - $7 is the FilePath column
awk -F',' 'NR==1 {print $0; next} !seen[$7]++' data.csv > data_nf.csv

# run nextflow
module load Nextflow
nextflow run code/nextflow/from_labid_table_test.nf
**/