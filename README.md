# astrocyte-differentiations

Background:
Differentiations of neuronal progenitor cells into astrocytes 

Aim:
Identify how many differentiated cells are astrocytes based on GFAP and ki67

## Sample

Fixed cultured neuronal progenitor cells.

## Staining & Microscopy

Confocal imaging: 
  - 6x z-stack per condition 
  - 43 z-planes

Three immunofluorescence channels: 
  - ch0: 647nm (ki67, nuclear, stains proliferating cells)
  - ch1: 488nm (GFAP, surrounds nucleus, stains astrocytes)
  - ch2: 405nm (DAPI, stains nuclei)


## [Data management](data_management.md)

## [Workflow](analysis_workflow.md)

## Publication guidelines

- Please follow the [guidelines for publishing bioimage data](https://www.nature.com/articles/s41592-023-01987-9.epdf).
- Please upload your imaging data on public archives (e.g. on the [BioImage Archive](https://www.ebi.ac.uk/bioimage-archive/)).
    - BioImage Archive help:
        - [Submission portal](https://www.ebi.ac.uk/bioimage-archive/submit/)
        - [Online tutorial](https://www.ebi.ac.uk/training/online/courses/bioimage-archive-quick-tour/)
        - Contacts:
            - [isabel.kemmer@embl.de](mailto:isabel.kemmer@embl.de)
            - [bioimage-archive@ebi.ac.uk](mailto:bioimage-archive@ebi.ac.uk)
- Please make your analysis workflow accessible (e.g. on [WorkflowHub](https://workflowhub.eu/)). 
- Please [acknowledge the support](https://git.embl.de/grp-cba/cba/-/blob/master/README.md#centre-for-bioimage-analysis-cba) of the Bioimage Informatics Service Team.